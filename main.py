#!/use/bin/env python
import numexpr
import sys
from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QLineEdit, QGridLayout, QPushButton,\
    QMessageBox, QWidgetAction

class LogicException(BaseException):
    pass

class Logic:
    def __init__(self):
        self.history = []
        self.is_last_expr = False

    def calculate(self, expression) -> str:
        try:
            result = numexpr.evaluate(expression)
        except ZeroDivisionError:
            raise LogicException("Cannot divide by 0!")
        except (ValueError, KeyError, SyntaxError) as e:
            raise LogicException("Incorrect expression: {}".format(str(e)))
        else:
            result = str(result)
            self.history.append(expression)
            self.history.append(result)
            self.is_last_expr = True
            return result

    def get_last_expression(self) -> str:
        try:
            if self.is_last_expr:
                self.history.pop()
                self.is_last_expr = False
            return self.history.pop()
        except IndexError:
            return ""


class Calculator(QMainWindow):
    button_labels = ['C', chr(8592), 'Undo',  '', '7', '8', '9', '/', '4', '5', '6', '*', '1', '2', '3', '-',
                     '0', '.', '=', '+']

    def __init__(self):
        super().__init__()

        self.__set_window_title()
        self.__set_central_widget()
        self.__create_field()
        self.__create_buttons()
        self.__create_menu_bar()
        self.logic = Logic()

    def __set_window_title(self):
        self.setWindowTitle("BI Calculator")

    def __set_central_widget(self):
        self.central_widget = QWidget(self)
        self.central_layout = QVBoxLayout(self.central_widget)
        self.central_widget.setLayout(self.central_layout)
        self.setCentralWidget(self.central_widget)

    def __create_field(self):
        self.field = QLineEdit(self.central_widget)
        self.field.setAlignment(Qt.Alignment.AlignRight)
        self.central_layout.addWidget(self.field)

    def __create_menu_bar(self):
        self.__menu_bar = self.menuBar()
        menu_item = self.__menu_bar.addMenu("BI Calculator")
        menu_item.addAction('Exit', self.close_app)

    @staticmethod
    def close_app():
        sys.exit(0)

    def __create_buttons(self):
        buttons_positions = [
            (0, 0), (0, 1), (0, 2), (0, 3),
            (1, 0), (1, 1), (1, 2), (1, 3),
            (2, 0), (2, 1), (2, 2), (2, 3),
            (3, 0), (3, 1), (3, 2), (3, 3),
            (4, 0), (4, 1), (4, 2), (4, 3)
        ]

        self.button_layout = QGridLayout(self.central_widget)

        for i, label in enumerate(self.button_labels):
            if not label:
                continue

            position = buttons_positions[i]

            button = QPushButton(label, self.central_widget)
            button.clicked.connect(self.__on_button_action(label))
            self.button_layout.addWidget(button, position[0], position[1])

        self.central_layout.addLayout(self.button_layout)

    def __on_button_action(self, label):
        def on_click():
            self.__on_button_clicked(label)
        return on_click

    def __on_button_clicked(self, button_label):
        if button_label == "C":
            self.field.setText("")
        elif button_label == chr(8592):
            text = self.field.text()
            self.field.setText(text[:-1])
        elif button_label == "Undo":
            restored_expression = self.logic.get_last_expression()
            self.field.setText(restored_expression)
        elif button_label == "=":
            text = self.field.text()
            try:
                result = self.logic.calculate(text)
            except LogicException as le:
                QMessageBox.critical(self, "Incorrect expression", "Error: {}".format(str(le)))
            else:
                self.field.setText(result)
        else:
            text = self.field.text()
            text += button_label
            self.field.setText(text)


if "__main__" == __name__:
    app = QApplication(sys.argv)
    window = Calculator()
    window.show()
    app.exec()
